import ipdb
import pathlib
import shutil
import os
import mdformat
from bs4 import BeautifulSoup
from datetime import datetime
from markdownify import MarkdownConverter, markdownify

from markdownify import MarkdownConverter

root = pathlib.Path("html_in")
out = pathlib.Path("md_out")

root.mkdir(exist_ok=True)
shutil.rmtree(out.name)
out.mkdir(exist_ok=True)
available_names = []

def date_from_soup(soup):
    date_soup = soup.find_all("h2", class_='date-header')
    if not date_soup:
        print(f"ERROR {item.name}")
        return
    return datetime.strptime(date_soup[0].string, '%A, %B %d, %Y').strftime('%Y-%m-%d')

def name_from_soup(soup):
    publication_date = date_from_soup(soup)
    return f"{publication_date}-{item.stem}.md"

def flatten_line(string):
    return ''.join(string.splitlines())

class HeadingConverter(MarkdownConverter):

    def convert_hn(self, n, el, text, convert_as_inline):
        return super().convert_hn(n, el, flatten_line(text.strip()), convert_as_inline)

    def convert_a(self, el, text, convert_as_inline):
        href = el.get('href')
        if not href:
            return super().convert_a(el, flatten_line(text.strip()), convert_as_inline)
        target = pathlib.Path(href).stem
        for name in available_names:
            if target in name:
                el["href"] = name
                break;

        return super().convert_a(el, flatten_line(text.strip()), convert_as_inline)

def frontmatter(title, date, tags):
    if os.getenv('PANDOC'):
        return f"## {title}\n\n"
    else:
        return f"""---
title: {title}
date: {date}
tags: [{', '.join(tags)}]
---

"""

for item in set(root.glob("**/**/*.html")) - set(root.glob("*.html"))- set(root.glob("p/*.html")):
    with item.open(mode="r") as html_source:
        content = html_source.read()
        soup = BeautifulSoup(content, "html.parser")
        md_name = name_from_soup(soup)
        if md_name:
            available_names.append(md_name)

for item in set(root.glob("**/**/*.html")) - set(root.glob("*.html"))- set(root.glob("p/*.html")):
    with item.open(mode="r") as html_source:
        content = html_source.read()
        soup = BeautifulSoup(content, "html.parser")
        md_name = name_from_soup(soup)
        publication_date = date_from_soup(soup)


        body = soup.find(itemprop='description articleBody')
        if not body:
            continue

        title = soup.find(itemprop="name").string.strip()
        tags = {a.string.strip() for a in soup.find_all("a", rel='tag')}
        body_md = HeadingConverter().convert(str(body))
        body_md = mdformat.text(body_md)
        content_md = frontmatter(title, publication_date, tags) + body_md


    out_path = (out / md_name)
    with out_path.open(mode="w")  as md_file:
        md_file.write(content_md)
