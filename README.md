
This repo contains a script to convert from blogspot html to hugo style
markdown.

After installing the requirements, you can run `python process.py`. The
script expect the scrapped html in `html_in` and outputs results in
`md_out`.
